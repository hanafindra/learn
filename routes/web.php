<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', 'LibraryController@login');
Route::get('/index', 'LibraryController@index');

Route::post('/tambah', 'LibraryController@tambah');
Route::get('/form', 'LibraryController@formTambah')->name('tambah.form');

Route::post('/update/{id}', 'LibraryController@update');
Route::get('/formEdit/{id}', 'LibraryController@formEdit');

Route::get('/hapus/{id}', 'LibraryController@hapus');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
