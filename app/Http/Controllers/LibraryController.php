<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class LibraryController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }
    
	public function login(){
		return view('auth.login');
	}

    public function index()
    {
    	$libs = \App\Library::all();
    	return view('pages.table', compact('libs'));
    }

    public function formTambah(){
    	return view('pages.formTambah');
    }

    public function tambah(Request $request){
    	$validate = \Validator::make($request->all(), 
    		[
    			'judul' => 'required',
    			'pengarang' => 'required',
    			'halaman' => 'required',
    			'penerbit' => 'required'
    		],

    		$after_save = [
    			'alert' => 'danger',
    			'title' => 'Terjadi Kesalahan!',
    			'text1' => 'Silahkan coba kembali!'
    		]);

    	if($validate->fails()){
    		return redirect()->back()->with('after_save', $after_save);
    	}

    	$after_save = [
    		'alert' => 'success',
    		'title' => 'Berhasil!',
    		'text1' => 'Data berhasil ditambah!'
    	];

    	$data = [
    		'judul' => $request->judul,
    		'pengarang' => $request->pengarang,
    		'halaman' => $request->halaman,
    		'penerbit' => $request->penerbit
    	];

    	$tambah = \App\Library::insert($data);
    	return redirect()->back()->with('after_save', $after_save);
    }


    //EDIT
      public function update(Request $request, $id)
     {
 
         $validate = \Validator::make($request->all(), [
                 'judul' => 'required',
                 'pengarang' => 'required',
                 'halaman' => 'required', 
                 'penerbit' => 'required'
             ], 
 
             $after_update = [
                 'alert' => 'danger',
                 'title' => 'Terjadi Kesalahan',
                 'text1' => 'Silahkan coba lagi!'
             ]);
 
         if($validate->fails()){
             return redirect()->back()->with('after_update', $after_update);
         }
 
         $after_update = [
             'alert' => 'success',
             'title' => 'Berhasil',
             'text1' => 'Update data berhasil'
         ];
 
         $data = [
             'judul' => $request->judul,
             'pengarang' => $request->pengarang,
             'halaman' => $request->halaman,
             'penerbit' => $request->penerbit
         ];
 
         $update = \App\Library::where('id', $id)->update($data);

         return redirect()->to('index')->with('after_update', $after_update);
     }


     //HAPUS
     public function hapus($id){
     	$library = \App\Library::Find($id);
     	$library->delete();

     	$after_delete = [
			'alert' => 'success',
			'title' => 'Berhasil',
			'text1' => 'Data berhasil dihapus'
        ];

     	return redirect("index")->with('after_delete', $after_delete);
     }
}
