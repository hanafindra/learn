<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Library extends Model
{
    protected $table = 'library';
    protected $fillable = [
    	'judul',
		'pengarang',
        'halaman',
        'penerbit',
		'created_at',
		'updated_at'
    ];

    
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
