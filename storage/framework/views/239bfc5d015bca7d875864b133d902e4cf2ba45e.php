<!DOCTYPE html>
<html lang="en">
<head>
  <?php echo $__env->make('includes.head', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</head>

<body id="mimin" class="dashboard">    
      <div class="container-fluid mimin-wrapper">
          <!-- start: Header -->
            <?php echo $__env->make('includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
          <!-- end: Header -->

          <!-- start: Content -->
            <?php echo $__env->yieldContent('content'); ?>
          <!-- end: content -->

          <!-- start:Left Menu -->
            <?php echo $__env->make('includes.leftMenu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
          <!-- end: Left Menu -->

          <!-- start: right menu -->
            <?php echo $__env->make('includes.rightMenu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
          <!-- end: right menu -->
          
      </div>

      <!-- start: Mobile -->
        <?php echo $__env->make('includes.menuMobile', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <!-- end: Mobile -->

<!-- start: Javascript -->
  <?php echo $__env->make('includes.foot', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<!-- end: Javascript -->
</body>
</html>