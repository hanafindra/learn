<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4>TAMBAH DATA</h4>
      </div>

      <form action="<?php echo e(URL('tambah')); ?>" method="POST" enctype="multipart/form-data">
      <div class="modal-body">    
        <?php echo e(csrf_field()); ?>

        <div class="form-group">
         <label>Judul</label>
         <input class="form-control" type="text" name="judul" placeholder="Masukan data kendaraan dengan benar!">
        </div>
        <div class="form-group">
         <label>Pengarang</label>
         <input class="form-control" type="text" name="pengarang" placeholder="Masukan data kendaraan dengan benar!">
        </div>
        <div class="form-group">
         <label>Halaman</label>
         <input class="form-control" type="text" name="halaman"
         placeholder="Masukan data kendaraan dengan benar!">
        </div>
        <div class="form-group">
         <label>Penerbit</label>
         <input class="form-control" type="text" name="penerbit"
         placeholder="Masukan data kendaraan dengan benar!">
        </div>
      </div>
      
      <div class="modal-footer">
        <button type="submit" class="btn btn-raised btn-primary pull-right">Submit</button>
        <button type="reset" class="btn btn-raised btn-warning pull-right">Reset</button>
      </div>

      </form>
    </div>

  </div>
</div>