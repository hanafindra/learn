<?php $__env->startSection('content'); ?>
    <div class="container">
     <div class="panel-body" style="margin: 50px;">
         <a href="<?php echo e(URL('index')); ?>" class="btn btn-raised btn-danger pull-left">Kembali</a>

         <div class="row">
             <div class="col-md-12"><hr>
                 <div class="col-md-2"></div>
                 <div class="col-md-8">
                     <form class="form-horizontal" action="<?php echo e(URL('update/'. $showById->id)); ?>" method="POST">
                     <?php echo e(csrf_field()); ?>

                       <fieldset>
                         <legend>FORM EDIT DATA</legend>
                             <div class="form-group label-floating">
                               <label class="control-label" for="focusedInput2">Judul</label>
                               <input class="form-control" id="focusedInput2" type="text" name="judul" value="<?php echo e($showById->judul); ?>">
                             </div>
                             <div class="form-group label-floating">
                               <label class="control-label" for="focusedInput2">Pengarang</label>
                               <input class="form-control" id="focusedInput2" type="text" name="pengarang" value="<?php echo e($showById->pengarang); ?>">
                             </div>
                             <div class="form-group label-floating">
                               <label class="control-label" for="focusedInput2">Halaman</label>
                               <input class="form-control" id="focusedInput2" type="text" name="halaman"
                               value="<?php echo e($showById->halaman); ?>">
                             </div>
                             <div class="form-group label-floating">
                               <label class="control-label" for="focusedInput2">Penerbit</label>
                               <input class="form-control" id="focusedInput2" type="text" name="penerbit"
                               value="<?php echo e($showById->penerbit); ?>">
                             </div>
                             <div class="form-group">
                               <div class="col-md-12">
                                 <button type="submit" class="btn btn-raised btn-primary pull-right">Submit</button>
                               </div>
                             </div>
                         </fieldset>
                     </form>
                 </div>
                 <div class="col-md-2"></div>
             </div>
            </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>