<!doctype html>
<html lang="<?php echo e(app()->getLocale()); ?>">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css">

        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>
    </head>
    <body>

    <div class="container">
     <div class="panel-body" style="margin: 50px;">
         <a href="<?php echo e(URL('index')); ?>" class="btn btn-raised btn-danger pull-left">Kembali</a>

         <div class="row">
             <div class="col-md-12"><hr>
                 <div class="col-md-2"></div>
                 <div class="col-md-8">
                     <form class="form-horizontal" action="<?php echo e(URL('update/'. $showById->id)); ?>" method="POST">
                     <?php echo e(csrf_field()); ?>

                       <fieldset>
                         <legend>FORM EDIT DATA</legend>
                             <div class="form-group label-floating">
                               <label class="control-label" for="focusedInput2">Judul</label>
                               <input class="form-control" id="focusedInput2" type="text" name="judul" value="<?php echo e($showById->judul); ?>">
                             </div>
                             <div class="form-group label-floating">
                               <label class="control-label" for="focusedInput2">Pengarang</label>
                               <input class="form-control" id="focusedInput2" type="text" name="pengarang" value="<?php echo e($showById->pengarang); ?>">
                             </div>
                             <div class="form-group label-floating">
                               <label class="control-label" for="focusedInput2">Halaman</label>
                               <input class="form-control" id="focusedInput2" type="text" name="halaman"
                               value="<?php echo e($showById->halaman); ?>">
                             </div>
                             <div class="form-group label-floating">
                               <label class="control-label" for="focusedInput2">Penerbit</label>
                               <input class="form-control" id="focusedInput2" type="text" name="penerbit"
                               value="<?php echo e($showById->penerbit); ?>">
                             </div>
                             <div class="form-group">
                               <div class="col-md-12">
                                 <button type="submit" class="btn btn-raised btn-primary pull-right">Submit</button>
                               </div>
                             </div>
                         </fieldset>
                     </form>
                 </div>
                 <div class="col-md-2"></div>
             </div>
            </div>
    </body>
</html>