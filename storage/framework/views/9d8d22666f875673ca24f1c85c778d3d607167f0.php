<?php $__env->startSection('content'); ?>
  <div id="content">
     <div class="panel box-shadow-none content-header">
        <div class="panel-body">
          <div class="col-md-12">
              <h3 class="animated fadeInLeft">Data Perpustakaan</h3>
              <p class="animated fadeInDown">
                Table <span class="fa-angle-right fa"></span> Data Perpustakaan
              </p>

              
                <?php echo $__env->make('includes.notif', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
              
          </div>
        </div>
    </div>

    <div class="col-md-12 top-20 padding-0">
      <div class="col-md-12">
        <div class="panel">
          <div class="panel-heading"><h3>Data Buku</h3>
            <a class="btn btn-primary" style="margin: 5px;" data-toggle="modal" data-target="#tambah">Tambah</a>
          </div>
          <div class="panel-body">
            <div class="responsive-table">
            <table id="datatables-example" class="table table-striped table-bordered" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th>#</th>
                <th>Judul</th>
                <th>Pengarang</th>
                <th>halaman</th>
                <th>Penerbit</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              <?php ($no = 1); ?>
              <?php $__currentLoopData = $libs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $lib): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              <tr>
                 <td><?php echo e($no++); ?></td>
                 <td><?php echo e($lib->judul); ?></td>
                 <td><?php echo e($lib->pengarang); ?></td>
                 <td><?php echo e($lib->halaman); ?></td>
                 <td><?php echo e($lib->penerbit); ?></td>
                 <td>
                     <center>
                        <a class="btn btn-sm btn-info tombol" data-toggle="modal" 
                        data-id="<?php echo e($lib->id); ?>"
                        data-judul="<?php echo e($lib->judul); ?>"
                        data-pengarang="<?php echo e($lib->pengarang); ?>"
                        data-halaman="<?php echo e($lib->halaman); ?>"
                        data-penerbit="<?php echo e($lib->penerbit); ?>"
                        data-target="#edit">Edit</a>
                        <a class="btn btn-danger tombol" data-id="<?php echo e($lib->id); ?>" data-toggle="modal" data-target="#hapus">Hapus</a>
                     </center>
                 </td>
              </tr>
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </tbody>
              </table>
            </div>
        </div>
      </div>
    </div>  
    </div>
  </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>