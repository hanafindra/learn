<!doctype html>
<html lang="<?php echo e(app()->getLocale()); ?>">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">

    </head>
    <body>
        <div class="container" style="margin-top: 50px;">

        <a href="<?php echo e(URL('form')); ?>" class="btn btn-primary" style="margin-bottom: 50px;">Tambah</a>

        
         <?php if(Session::has('after_update')): ?>
             <div class="row" style="margin: 20px;">
                 <div class="col-md-12">
                     <div class="alert alert-dismissible alert-<?php echo e(Session::get('after_update.alert')); ?>">
                       <button type="button" class="close" data-dismiss="alert">×</button>
                       <strong><?php echo e(Session::get('after_update.title')); ?></strong>
                       <a href="javascript:void(0)" class="alert-link"><?php echo e(Session::get('after_update.text1')); ?></a>
                     </div>
                 </div>
             </div>
         <?php endif; ?>
         

        <table id="myTable" class="table table-bordered table-hover">
             <thead>
                 <tr>
                     <th>#</th>
                     <th>Judul</th>
                     <th>Pengarang</th>
                     <th>halaman</th>
                     <th>Penerbit</th>
                     <th>Aksi</th>
                 </tr>
             </thead>
             <tbody>
                 <?php ($no = 1); ?>
                 <?php $__currentLoopData = $libs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $lib): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                     <tr>
                         <td><?php echo e($no++); ?></td>
                         <td><?php echo e($lib->judul); ?></td>
                         <td><?php echo e($lib->pengarang); ?></td>
                         <td><?php echo e($lib->halaman); ?></td>
                         <td><?php echo e($lib->penerbit); ?></td>
                         <td>
                             <center>
                                 <a href="<?php echo e(URL('formEdit')); ?>/<?php echo e($lib->id); ?>" class="btn btn-sm btn-raised btn-info">Edit</a>
                                 <a href="<?php echo e(URL('hapus')); ?>/<?php echo e($lib->id); ?>" class="btn btn-sm btn-raised btn-danger">Hapus</a>
                             </center>
                         </td>
                     </tr>
                 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                 
             </tbody>
        </table>
        </div>

    </body>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>
    <script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js" type="text/javascript"></script>

    <script type="text/javascript">
      $(document).ready(function(){
          $('#myTable').DataTable();
      });
    </script>
</html>