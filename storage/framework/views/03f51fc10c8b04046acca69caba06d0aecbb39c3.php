<!doctype html>
<html lang="<?php echo e(app()->getLocale()); ?>">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css">

        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>
    </head>
    <body>
        <div class="container" style="margin-top: 50px;">

        <a href="<?php echo e(URL('blog/create')); ?>" class="btn btn-primary" style="margin-bottom: 50px;">Tambah</a>
        <table class="table table-bordered table-hover">
             <thead>
                 <tr>
                     <th>#</th>
                     <th>Title</th>
                     <th>Deskripsi</th>
                     <th>Aksi</th>
                 </tr>
             </thead>
             <tbody>
                 <?php ($no = 1); ?>
                 <?php $__currentLoopData = $blogs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $blog): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                     <tr>
                         <td><?php echo e($no++); ?></td>
                         <td><?php echo e($blog->title); ?></td>
                         <td><?php echo e($blog->description); ?></td>
                         <td>
                             <center>
                                 <a href="<?php echo e(URL('blog/show')); ?>/<?php echo e($blog->id); ?>" class="btn btn-sm btn-raised btn-info">Edit</a>
                                 <a href="<?php echo e(URL('blog/destroy')); ?>/<?php echo e($blog->id); ?>" class="btn btn-sm btn-raised btn-danger">Hapus</a>
                             </center>
                         </td>
                     </tr>
                 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                 
             </tbody>
        </table>
        </div>

    </body>
</html>