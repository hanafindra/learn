<?php if(Session::has('after_update')): ?>
   <div class="row" style="margin: 20px;">
       <div class="col-md-12">
           <div class="alert alert-dismissible alert-<?php echo e(Session::get('after_update.alert')); ?>">
             <button type="button" class="close" data-dismiss="alert">×</button>
             <strong><?php echo e(Session::get('after_update.title')); ?></strong>
             <a href="javascript:void(0)" class="alert-link"><?php echo e(Session::get('after_update.text1')); ?></a>
           </div>
       </div>
   </div>
<?php endif; ?>

<?php if(Session::has('after_save')): ?>
   <div class="row">
       <div class="col-md-12">
           <div class="alert alert-dismissible alert-<?php echo e(Session::get('after_save.alert')); ?>">
             <button type="button" class="close" data-dismiss="alert">×</button>
             <strong><?php echo e(Session::get('after_save.title')); ?></strong>
             <a href="javascript:void(0)" class="alert-link"><?php echo e(Session::get('after_save.text1')); ?></a>
           </div>
       </div>
   </div>
<?php endif; ?>

<?php if(Session::has('after_delete')): ?>
   <div class="row">
       <div class="col-md-12">
           <div class="alert alert-dismissible alert-<?php echo e(Session::get('after_delete.alert')); ?>">
             <button type="button" class="close" data-dismiss="alert">×</button>
             <strong><?php echo e(Session::get('after_delete.title')); ?></strong>
             <a href="javascript:void(0)" class="alert-link"><?php echo e(Session::get('after_delete.text1')); ?></a>
           </div>
       </div>
   </div>
<?php endif; ?>