<?php $__env->startSection('content'); ?>
    
        <a href="<?php echo e(URL('form')); ?>" class="btn btn-primary" style="margin-bottom: 50px;">Tambah</a>

        
         <?php if(Session::has('after_update')): ?>
             <div class="row" style="margin: 20px;">
                 <div class="col-md-12">
                     <div class="alert alert-dismissible alert-<?php echo e(Session::get('after_update.alert')); ?>">
                       <button type="button" class="close" data-dismiss="alert">×</button>
                       <strong><?php echo e(Session::get('after_update.title')); ?></strong>
                       <a href="javascript:void(0)" class="alert-link"><?php echo e(Session::get('after_update.text1')); ?></a>
                     </div>
                 </div>
             </div>
         <?php endif; ?>
         

        <table class="table table-bordered table-hover mytable">
             <thead>
                 <tr>
                     <th>#</th>
                     <th>Judul</th>
                     <th>Pengarang</th>
                     <th>halaman</th>
                     <th>Penerbit</th>
                     <th>Aksi</th>
                 </tr>
             </thead>
             <tbody>
                 <?php ($no = 1); ?>
                 <?php $__currentLoopData = $libs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $lib): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                     <tr>
                         <td><?php echo e($no++); ?></td>
                         <td><?php echo e($lib->judul); ?></td>
                         <td><?php echo e($lib->pengarang); ?></td>
                         <td><?php echo e($lib->halaman); ?></td>
                         <td><?php echo e($lib->penerbit); ?></td>
                         <td>
                             <center>
                                 <a href="<?php echo e(URL('formEdit')); ?>/<?php echo e($lib->id); ?>" class="btn btn-sm btn-raised btn-info">Edit</a>
                                 <a href="<?php echo e(URL('hapus')); ?>/<?php echo e($lib->id); ?>" class="btn btn-sm btn-raised btn-danger">Hapus</a>
                             </center>
                         </td>
                     </tr>
                 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                 
             </tbody>
        </table>

        <script type="text/javascript">
          $(document).ready(function(){
              $('.mytable').DataTable();
          });
        </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>