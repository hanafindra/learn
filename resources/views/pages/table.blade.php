@extends('layouts.default')
@section('content')
  <div id="content">
     <div class="panel box-shadow-none content-header">
        <div class="panel-body">
          <div class="col-md-12">
              <h3 class="animated fadeInLeft">Data Perpustakaan</h3>
              <p class="animated fadeInDown">
                Table <span class="fa-angle-right fa"></span> Data Perpustakaan
              </p>

              {{-- part alert --}}
                @include('includes.notif')
              {{-- end part alert --}}
          </div>
        </div>
    </div>

    <div class="col-md-12 top-20 padding-0">
      <div class="col-md-12">
        <div class="panel">
          <div class="panel-heading"><h3>Data Buku</h3>
            <a class="btn btn-primary" style="margin: 5px;" data-toggle="modal" data-target="#tambah">Tambah</a>
          </div>
          <div class="panel-body">
            <div class="responsive-table">
            <table id="datatables-example" class="table table-striped table-bordered" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th>#</th>
                <th>Judul</th>
                <th>Pengarang</th>
                <th>halaman</th>
                <th>Penerbit</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              @php($no = 1)
              @foreach ($libs as $lib)
              <tr>
                 <td>{{ $no++ }}</td>
                 <td>{{ $lib->judul }}</td>
                 <td>{{ $lib->pengarang }}</td>
                 <td>{{ $lib->halaman }}</td>
                 <td>{{ $lib->penerbit }}</td>
                 <td>
                     <center>
                        <a class="btn btn-sm btn-info tombol" data-toggle="modal" 
                        data-id="{{ $lib->id }}"
                        data-judul="{{ $lib->judul }}"
                        data-pengarang="{{ $lib->pengarang }}"
                        data-halaman="{{ $lib->halaman }}"
                        data-penerbit="{{ $lib->penerbit }}"
                        data-target="#edit">Edit</a>
                        <a class="btn btn-danger tombol" data-id="{{ $lib->id }}" data-toggle="modal" data-target="#hapus">Hapus</a>
                     </center>
                 </td>
              </tr>
              @endforeach
            </tbody>
              </table>
            </div>
        </div>
      </div>
    </div>  
    </div>
  </div>
@endsection