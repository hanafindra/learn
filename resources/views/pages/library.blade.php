@extends('layouts.default')
@section('content')
    
        <a href="{{ URL('form') }}" class="btn btn-primary" style="margin-bottom: 50px;">Tambah</a>

        {{-- part alert --}}
         @if (Session::has('after_update'))
             <div class="row" style="margin: 20px;">
                 <div class="col-md-12">
                     <div class="alert alert-dismissible alert-{{ Session::get('after_update.alert') }}">
                       <button type="button" class="close" data-dismiss="alert">×</button>
                       <strong>{{ Session::get('after_update.title') }}</strong>
                       <a href="javascript:void(0)" class="alert-link">{{ Session::get('after_update.text1') }}</a>
                     </div>
                 </div>
             </div>
         @endif
         {{-- end part alert --}}

        <table class="table table-bordered table-hover mytable">
             <thead>
                 <tr>
                     <th>#</th>
                     <th>Judul</th>
                     <th>Pengarang</th>
                     <th>halaman</th>
                     <th>Penerbit</th>
                     <th>Aksi</th>
                 </tr>
             </thead>
             <tbody>
                 @php($no = 1)
                 @foreach ($libs as $lib)
                     <tr>
                         <td>{{ $no++ }}</td>
                         <td>{{ $lib->judul }}</td>
                         <td>{{ $lib->pengarang }}</td>
                         <td>{{ $lib->halaman }}</td>
                         <td>{{ $lib->penerbit }}</td>
                         <td>
                             <center>
                                 <a href="{{ URL('formEdit') }}/{{ $lib->id }}" class="btn btn-sm btn-raised btn-info">Edit</a>
                                 <a href="{{ URL('hapus') }}/{{ $lib->id }}" class="btn btn-sm btn-raised btn-danger">Hapus</a>
                             </center>
                         </td>
                     </tr>
                 @endforeach
                 {{-- // end loop --}}
             </tbody>
        </table>

        <script type="text/javascript">
          $(document).ready(function(){
              $('.mytable').DataTable();
          });
        </script>
@endsection