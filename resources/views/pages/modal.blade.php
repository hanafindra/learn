<!-- Modal -->
<div class="modal fade" id="hapus" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Hapus Data</h4>
      </div>
      <div class="modal-body">
        <div class="alert alert-warning">
          <strong>Perhatian!</strong> Apakah anda yakin ingin menghapusnya?
        </div>
      </div>
      <div class="modal-footer">
        <input class="id" type="hidden" name="id">
        <a href="{{ URL('hapus') }}/{{ $lib->id }}" class="btn btn-sm btn-danger">Hapus</a>
      </div>
    </div>
  </div>
</div>

<div id="tambah" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4>TAMBAH DATA</h4>
      </div>

      <form action="{{ URL('tambah') }}" method="POST" enctype="multipart/form-data">
      <div class="modal-body">    
        {{ csrf_field() }}
        <div class="form-group">
         <label>Judul</label>
         <input class="form-control" type="text" name="judul" placeholder="Masukan data kendaraan dengan benar!">
        </div>
        <div class="form-group">
         <label>Pengarang</label>
         <input class="form-control" type="text" name="pengarang" placeholder="Masukan data kendaraan dengan benar!">
        </div>
        <div class="form-group">
         <label>Halaman</label>
         <input class="form-control" type="text" name="halaman"
         placeholder="Masukan data kendaraan dengan benar!">
        </div>
        <div class="form-group">
         <label>Penerbit</label>
         <input class="form-control" type="text" name="penerbit"
         placeholder="Masukan data kendaraan dengan benar!">
        </div>
      </div>
      
      <div class="modal-footer">
        <button type="submit" class="btn btn-raised btn-primary pull-right">Submit</button>
        <button type="reset" class="btn btn-raised btn-warning pull-right">Reset</button>
      </div>

      </form>
    </div>

  </div>
</div>


<!-- EDIT -->
<div id="edit" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4>TAMBAH DATA</h4>
      </div>

      <form action="{{ URL('update/'. $lib->id) }}" method="POST">
      <div class="modal-body">    
        {{ csrf_field() }}
        <div class="form-group">
         <label>Judul</label>
         <input class="form-control judul" type="text" name="judul">
        </div>
        <div class="form-group">
         <label>Pengarang</label>
         <input class="form-control peng" type="text" name="pengarang">
        </div>
        <div class="form-group">
         <label>Halaman</label>
         <input class="form-control hal" type="text" name="halaman">
        </div>
        <div class="form-group">
         <label>Penerbit</label>
         <input class="form-control penerbit" type="text" name="penerbit">
        </div>
      </div>
      
      <div class="modal-footer">
        <button type="submit" class="btn btn-raised btn-primary pull-right">Submit</button>
      </div>

      </form>
    </div>

  </div>
</div>