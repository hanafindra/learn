<!DOCTYPE html>
<html lang="en">
<head>
  @include('includes.head')
</head>

<body id="mimin" class="dashboard">    
      <div class="container-fluid mimin-wrapper">
          <!-- start: Header -->
            @include('includes.header')
          <!-- end: Header -->

          <!-- start:Left Menu -->
            @include('includes.leftMenu')
          <!-- end: Left Menu -->

          <!-- start: Content -->
            @yield('content')
          <!-- end: content -->

          <!-- start: right menu -->
            @include('includes.rightMenu')
          <!-- end: right menu -->

          <!-- start: Modals -->
            @include('pages.modal')
          <!-- end: Modals -->
          
      </div>

      <!-- start: Mobile -->
        @include('includes.mobileMenu')
      <!-- end: Mobile -->

<!-- start: Javascript -->
  @include('includes.foot')
<!-- end: Javascript -->
</body>
</html>