<meta charset="utf-8">
<meta name="description" content="Miminium Admin Template v.1">
<meta name="author" content="Isna Nur Azis">
<meta name="keyword" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Miminium</title>

<!-- start: Css -->
<link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/css/bootstrap.min.css') }}">

<!-- plugins -->
<link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/css/plugins/font-awesome.min.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/css/plugins/datatables.bootstrap.min.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/css/plugins/animate.min.css') }}"/>
<link href="{{ URL::asset('admin/css/style.css') }}" rel="stylesheet">
<!-- end: Css -->

<link rel="shortcut icon" href="{{ URL::asset('admin/img/logomi.png') }}">