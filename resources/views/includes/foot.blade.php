<script src="{{ URL::asset('admin/js/jquery.min.js') }}"></script>
<script src="{{ URL::asset('admin/js/jquery.ui.min.js') }}"></script>
<script src="{{ URL::asset('admin/js/bootstrap.min.js') }}"></script>



<!-- plugins -->
<script src="{{ URL::asset('admin/js/plugins/moment.min.js') }}"></script>
<script src="{{ URL::asset('admin/js/plugins/jquery.datatables.min.js') }}"></script>
<script src="{{ URL::asset('admin/js/plugins/datatables.bootstrap.min.js') }}"></script>
<script src="{{ URL::asset('admin/js/plugins/jquery.nicescroll.js') }}"></script>


<!-- custom -->
<script src="{{ URL::asset('admin/js/main.js') }}"></script>
<script src="{{ URL::asset('admin/js/custom.js') }}"></script>

<script type="text/javascript">
  $(document).ready(function(){
    $('#datatables-example').DataTable();
  });
</script>